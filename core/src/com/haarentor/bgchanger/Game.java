package com.haarentor.bgchanger;

import com.badlogic.gdx.ApplicationAdapter;
import com.haarentor.bgchanger.managers.GameStateManager;

/* @date: 09-02-2016
 * @author: fSobing
 * Die Game-Klasse ist die Hauptklasse aus dem Core-Projekt.
 * Sie initialisiert die Hilfsklassen(-manager, services, etc)
 * des weiteren bestimmt sie welche Klassen in den 
 * Prozessen der Methoden eingebunden werden. 
 */
public class Game extends ApplicationAdapter {
	
	// Private Variable des Typ GameStateManagers
	private GameStateManager gsm;
	
	@Override
	public void create () {
		// Erstellt eine neue Instanz des GameStateManagers
		this.gsm = new GameStateManager();
	}


	/*
	 * (non-Javadoc)
	 * @see com.badlogic.gdx.ApplicationAdapter#render()
	 * Die Render-Methode dient zum Zeichnen der einzelnen Objekten.
	 * Sie wird ~60x in der Sekunde aufgerufen.
	 */
	@Override	
	public void render () {
		this.gsm.render();
	}
}
