package com.haarentor.bgchanger.managers;

import java.util.Stack;

import com.badlogic.gdx.Gdx;

/*
 * @author fSobing
 * Diese (Hilfs)-Klasse dient zur Verwaltung der einzelnen States.
 * �blicherweise geh�rt zu diesen States ein PlayState, MenuState, TutorialState etc.
 */
public class GameStateManager {
	// Die einzelnen States werden in einem Stack gelagert.
	private Stack<GameState> gameStates;
	
	// Jedes State/Fenster hat eine eindeutige ID.
	public static final int MENU = 68235985;
	
	
	public GameStateManager(){
		this.gameStates = new Stack<GameState>();
		this.pushState(MENU);
		
	}
	
	public void update(float dt) {
		gameStates.peek().update(dt);
	}
	
	// Render-Methode wird dem aktuellen State �bergeben
	public void render(){
		this.update(Gdx.graphics.getDeltaTime());
		gameStates.peek().render();
	}
	
	// Hier wird mit Hilfe der �bergebenen Zahl der State festgelegt.
	public void setState(int state){
		popState();
		pushState(state);
	}
	
	// State aktualisieren
	private void pushState(int state){
		gameStates.push(getState(state));
	}
	
	// State ausblenden
	private void popState() {
		GameState g = gameStates.pop();
		g.dispose();		
	}
	
	// Diese Methode gibt den aktuellen State zur�ck
	private GameState getState(int state){
		switch(state){
		case MENU:
			return new MenuGameState();	
	    default:
	    	return null;
		}
	}
	
}
