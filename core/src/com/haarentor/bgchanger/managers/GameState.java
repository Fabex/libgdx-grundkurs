package com.haarentor.bgchanger.managers;

// Gibt die Methoden f�r ein State vor, der von dieser Klasse erbt.
public abstract class GameState {
	
	abstract void dispose();
	abstract void render();
	abstract void update(float dt);
}
