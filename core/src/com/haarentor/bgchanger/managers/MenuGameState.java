package com.haarentor.bgchanger.managers;

import com.badlogic.gdx.Gdx;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector3;

public class MenuGameState extends GameState {

	private Rectangle rect;
	private ShapeRenderer shapeRenderer;
	private OrthographicCamera cam;
	private Color bgColorBlue;
	private Color bgColorRed;
	
	private boolean bgChangerFlag;
	public MenuGameState()
	{
		cam = new OrthographicCamera();
		cam.setToOrtho(false, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		
		shapeRenderer = new ShapeRenderer();
		
		// Initialisierung des Rechtecks
		rect = new Rectangle();
		rect.setWidth(150);
		rect.setHeight(100);
		rect.setX(Gdx.graphics.getWidth() / 2 - (rect.getWidth() / 2));
		rect.setY(Gdx.graphics.getHeight() /2 - (rect.getHeight() / 2));
		
		/* Hier werden RGBA-Farbcodes verwendet.
		 * Wichtig: WERT / 255
		*/		
		bgColorBlue = new Color(0, 0, 1, 1);
		bgColorRed = new Color(1, 0, 0, 1);
	}
	
	@Override
	void render() {
		/* Wichtig: Dinge wie Hintergrundfarbe, in 
		 * den oberen Zeilen der Render-Methode schreiben, sonst
		 * �berlappen sich die Zeichnungen.
		 */
		if(bgChangerFlag){
			Gdx.gl.glClearColor(bgColorBlue.r, bgColorBlue.g, bgColorBlue.b, bgColorBlue.a);
		}
		else{
			Gdx.gl.glClearColor(bgColorRed.r, bgColorRed.g, bgColorRed.b, bgColorRed.a);
		}
		
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);	
		
		// ShapeRender Block - Dient zum Zeichnen von Shapes (Symbolen)
		shapeRenderer.setColor(Color.BLACK);
		shapeRenderer.begin(ShapeType.Filled);
		shapeRenderer.rect(rect.getX(), rect.getY(), rect.getWidth(), rect.getHeight());
		shapeRenderer.end();
	}

	private void handleInput() {
		if (Gdx.input.justTouched()) {
			Vector3 touchPos = new Vector3();
			touchPos.set(Gdx.input.getX(), Gdx.input.getY(), 0);
			cam.unproject(touchPos);
			
			if(rect.contains(touchPos.x, touchPos.y))
			{
				// F�hre eine Methode aus..
				bgChangerFlag = !bgChangerFlag;
				
				/* //M�gliche �nderung
				rect.setY(rect.getY() * 1.15f);
				rect.setSize(50,150);
				rect.setX(Gdx.graphics.getWidth() / 2 - (rect.getWidth() / 2));
				*/
			}
		}
	}

	/*
	 * Wird stetig aufgerufen.
	 */
	@Override
	void update(float dt) {
		this.handleInput();
	}	

	@Override
	void dispose() {

	}


}
